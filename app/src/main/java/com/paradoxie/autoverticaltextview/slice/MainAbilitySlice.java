package com.paradoxie.autoverticaltextview.slice;

import com.paradoxie.autoscrolltextview.VerticalTextview;
import com.paradoxie.autoverticaltextview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.OnClickListener;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

import java.util.ArrayList;

/**
 * MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {

    private VerticalTextview TextView;
    private ArrayList<String> titleList = new ArrayList<String>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        init();

    }

    /**
     * init
     */
    public void init() {
        TextView = (VerticalTextview) findComponentById(ResourceTable.Id_verticalText);
        titleList.add("你是天上最受111");
        titleList.add("我是丑人脸上22的鼻上的鼻上的鼻涕222");
        titleList.add("你发出完美的声音333");
        titleList.add("我被默默揩去44我被默默揩去444");
        titleList.add("你冷酷外表画意555");
        titleList.add("我已经够胖还胖还胖还吃东西666");
//        titleList.add("你踏着七彩祥云离去777");
//        titleList.add("我被留在这里888");
        TextView.setTextList(titleList);
        TextView.setText(56, 10, Color.BLUE.getValue());//设置属性
        TextView.setTextStillTime(1500);//设置停留时长间隔
        TextView.setAnimTime(1000);//设置进入和退出的时间间隔
        TextView.setOnItemClickListener(new VerticalTextview.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                ResUtils.showToast(getContext(),"点击了 : " + titleList.get(position));
            }
        });


    }

    @Override
    public void onActive() {
        super.onActive();
        TextView.startAutoScroll();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        TextView.stopAutoScroll();
    }
}
