/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.paradoxie.autoverticaltextview.slice;

import com.paradoxie.autoverticaltextview.MyApplication;
import com.paradoxie.autoverticaltextview.ResourceTable;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 资源工具类
 */
public class ResUtils {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.DEBUG, 0x00201, "TAG");


    /**
     * LogE
     * @param format
     * @param args
     */
    public static void LogE(String format, Object... args){
        HiLog.error(LABEL,format,args);
    }

    /**
     * LogLine
     */
    public static void LogLine(){
        LogLine("");
    }

    /**
     * LogLine
     * @param log
     */
    public static void LogLine(String log){
        HiLog.error(LABEL,"======================="+log+"============================");
    }


    /**
     * showToast
     * @param msg
     */
    public static void showToast(Context context,String msg){
        DirectionalLayout layout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_layout_toast, null, false);
        Text text = (Text) layout.findComponentById(ResourceTable.Id_msg_toast);
        text.setText(msg);
        new ToastDialog(context)
                .setComponent(layout)
                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.CENTER)
                .show();
    }
}
