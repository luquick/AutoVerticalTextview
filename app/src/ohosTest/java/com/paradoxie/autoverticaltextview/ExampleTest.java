package com.paradoxie.autoverticaltextview;

import com.paradoxie.autoscrolltextview.TextUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ExampleTest {


    @Test
    public void TextUtils2() {
        boolean isEmpty =  TextUtils.isEmpty("12");
        System.out.println(isEmpty);
    }
    @Test
    public void TextUtils21() {
        boolean isEmpty =  TextUtils.isEmpty("");
        System.out.println(isEmpty);
    }
    @Test
    public void TextUtils22() {
        boolean isEmpty =  TextUtils.isEmpty(null);
        System.out.println(isEmpty);
    }

    @Test
    public void TextUtils23() {
        List list = new ArrayList();
        boolean isEmpty =  TextUtils.isEmpty(list);
        System.out.println(isEmpty);
    }

    @Test
    public void TextUtils24() {
        List list = new ArrayList();
        list.add("1");
        boolean isEmpty =  TextUtils.isEmpty(list);
        System.out.println(isEmpty);
    }

}
