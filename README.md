# AutoVerticalTextview
 
#### 项目介绍
- 项目名称：AutoVerticalTextview
- 所属系列：openharmony的第三方组件适配移植
- 功能：垂直滚动的textview
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 0.1


#### 效果演示

![效果演示](./printscreen/autoverticaltextview.gif "截图")


#### 安装教程


1.在项目根目录下的build.gradle文件中，

 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:AutoVerticalTextview:0.0.1-SNAPSHOT')
    ......  
 }
 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

将自定义view添加到XML中，使用方法如下:
```
<com.paradoxie.autoscrolltextview.VerticalTextview
    ohos:id="$+id:verticalText"
    ohos:width="260vp"
    ohos:height="match_content"
    ohos:background_element="$graphic:background_btn_element"
    />
```

完整调用:

```
        ArrayList<String> titleList = new ArrayList<String>();
        titleList.add("你是天上最受111");
        titleList.add("我是丑人脸上22的鼻上的鼻上的鼻涕222");
        titleList.add("你发出完美的声音333");
        titleList.add("我被默默揩去44我被默默揩去444");
        titleList.add("你冷酷外表画意555");
        titleList.add("我已经够胖还胖还胖还吃东西666");

        VerticalTextview TextView = (VerticalTextview)findComponentById(ResourceTable.Id_verticalText);
        TextView.setTextList(titleList);
        TextView.setText(56, 10,Color.BLUE.getValue());//设置属性
        TextView.setTextStillTime(1500);//设置停留时长间隔
        TextView.setAnimTime(1000);//设置进入和退出的时间间隔
        TextView.setOnItemClickListener(new VerticalTextview.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                    ResUtils.showToast("点击了 : " + titleList.get(position));
            }
        });
```




| Method                      | Description                      |
| :-------------------------- | :------------------------------- |
| setTextList                | 设置显示文字                       |
| setText                    | 设置显示文字大小、内边距、文字颜色    |
| setAnimTime                | 设置进入和退出的时间间隔             |
| setOnItemClickListener     | 设置点击事件                        |
| startAutoScroll            | 设置开始滚动                        | 
| stopAutoScroll             | 设置停止滚动                        | 
 
 


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 0.0.1-SNAPSHOT

