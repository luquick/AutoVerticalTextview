/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.paradoxie.autoscrolltextview;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *   判空相关工具类
 */
public class TextUtils {

    private TextUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     *
     * 解决编码问题，去掉换行
     * 方法名称:replaceBlank
     * 方法描述:
     * @param str
     * @return 返回值描述
     */
    public static String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }

    /**
     *
     * 判断对象是否为空
     * @param obj 对象
     * @return 为空
     */
    public static boolean isEmpty(Object obj) {
        boolean flag = false;
        if (obj == null) return true;
        if(isEmptyToObj(obj)) flag = true;
        if(isEmptyToList(obj)) flag = true;
        return flag;

    }

    /**
     * isEmptyToObj
     *
     * @param obj
     * @return boolean
     */
    public static boolean isEmptyToObj(Object obj){
        if (obj instanceof Collection && ((Collection) obj).isEmpty()
            || obj instanceof String && obj.toString().length() == 0){
            return true;
        }

        return false;
    }

    /**
     * isEmptyToList
     *
     * @param obj
     * @return boolean
     */
    public static boolean isEmptyToList(Object obj){
        boolean flag = false;

        if (obj.getClass().isArray() && Array.getLength(obj) == 0){
            flag = true;
        }
        if (obj instanceof Map && ((Map) obj).isEmpty()){
            flag = true;
        }
        if (obj instanceof List && ((List) obj).size() == 0){
            flag = true;
        }
        return flag;
    }

    /**
     * 判断对象是否非空
     *
     * @param obj 对象
     * @return  非空
     */
    public static boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

}
